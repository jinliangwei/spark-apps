#!/usr/bin/env bash

# This file contains environment variables required to run Spark. Copy it as
# spark-env.sh and edit that to configure Spark for your site. At a minimum,
# the following two variables should be set:
# - MESOS_NATIVE_LIBRARY, to point to your Mesos native library (libmesos.so)
# - SCALA_HOME, to point to your Scala installation
#
# If using the standalone deploy mode, you can also set variables for it:
# - SPARK_MASTER_IP, to bind the master to a different IP address
# - SPARK_MASTER_PORT / SPARK_MASTER_WEBUI_PORT, to use non-default ports
# - SPARK_WORKER_CORES, to set the number of cores to use on this machine
# - SPARK_WORKER_MEMORY, to set how much memory to use (e.g. 1000m, 2g)
# - SPARK_WORKER_PORT / SPARK_WORKER_WEBUI_PORT
#
# Finally, Spark also relies on the following variables, but these can be set
# on just the *master* (i.e. in your driver program), and will automatically
# be propagated to workers:
# - SPARK_MEM, to change the amount of memory used per node (this should
#   be in the same format as the JVM's -Xmx option, e.g. 300m or 1g)
# - SPARK_CLASSPATH, to add elements to Spark's classpath
# - SPARK_JAVA_OPTS, to add JVM options
# - SPARK_LIBRARY_PATH, to add extra search paths for native libraries.

SCALA_HOME="/usr/share/scala-2.9.2"
SPARK_LAUNCH_WITH_SCALA="1"

# USE WHEN RUNNING LOCALLY START 
#SPARK_CLASSPATH="/home/jinliang/Work/research/code/biglearning_spark.jar"
#SPARK_CLASSPATH+=":/home/jinliang/Work/lib/lib.jar.git/breeze-learn-assembly-0.2-SNAPSHOT.jar"
# USE WHEN RUNNING LOCALLY END

# USE WHEN RUNNING ON CLUSTER START
SPARK_CLASSPATH="/home/user/jinlianw/ml-framework/progs/biglearning_spark.jar"
SPARK_CLASSPATH+=":/usr/share/breeze/breeze-learn-assembly-0.2-SNAPSHOT.jar"
# USE WHEN RUNNING ON CLUSTER END

SPARK_MASTER_IP="172.19.149.10"
SPARK_WORKER_CORES="1"
SPARK_WORKER_MEMORY="1g" # memory avaiable for spark app on each worker node

SPARK_MEM="1g"

# Per-machine property
SPARK_LOCAL_IP="172.19.149.10"