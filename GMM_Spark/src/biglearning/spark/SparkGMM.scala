package biglearning.spark

import java.util.Random
import spark.SparkContext
import scala.collection.mutable.HashMap
import scala.collection.mutable.LinkedList
import scala.Math
import spark.util.Vector
import breeze.linalg.DenseVector
import breeze.linalg.DenseMatrix
import breeze.linalg.LinearAlgebra
import scala.util.control.Breaks.break
import scala.util.control.Breaks.breakable
import java.io.File
import java.io.FileWriter
import java.lang.management.ManagementFactory
import java.lang.management.ThreadMXBean

/* A parallel GMM over Spark
 * Usage:
 * 1)  
 * 1) read input file, => RDD[String]
 * 2) map file to dataset => RDD[DenseVector]
 * 3)  
 * */

object SparkGMM {
    
    /* 
     * Given a string of numbers, convert it to a vector of doubles
     *	*/
    def string2Vector(line: String): DenseVector[Double] = {
        var s_array = line.trim.split("\\s+")
        var n_array = s_array.map(_.toDouble)
        new DenseVector(n_array)
    }
    def logJointGaussianPdf(x: DenseVector[Double], mu: DenseVector[Double], sigma_inverse: DenseMatrix[Double], sigma_det: Double):Double = {
        var x_minus_mu = x - mu
        
        var numerator = ((x_minus_mu.t)*sigma_inverse*x_minus_mu).data(0)
        -(numerator + x.length*Math.log(2*Math.Pi) + Math.log(sigma_det))/2        
    }
    
    /**
     * For each point x, return a tuple
     * (x, 
     * array of responsibility of each cluster for x, (rho)
     * x*rho)
     * 
     * */
    
    def newResponsibilityEachX(x: DenseVector[Double], 
            				   priors: Array[Double],
            				   mu: Array[DenseVector[Double]],
            				   sigma_inverse: Array[DenseMatrix[Double]],
            				   sigma_det: Array[Double]) : 
    						  (DenseVector[Double], Array[Double], Array[DenseVector[Double]], Double) = {
        
        var log_gaussian_prior_array = new Array[Double](mu.length) // Array of log gaussian pdf times prior
        var max_log_gaussian_prior = 0.0
        
        for(i <- 0 to (mu.length - 1)){
        	var log_gaussian = logJointGaussianPdf(x, mu(i), sigma_inverse(i), sigma_det(i))
        	log_gaussian_prior_array(i) = log_gaussian + Math.log(priors(i))
        	if(i == 0){
        	    max_log_gaussian_prior = log_gaussian_prior_array(i)
        	}else{
        	    if(log_gaussian_prior_array(i) > max_log_gaussian_prior){
        	        max_log_gaussian_prior = log_gaussian_prior_array(i) 
        	    }
        	}
        }
        
        var rho_div_rho_max_array = log_gaussian_prior_array.map(t => Math.exp(t - max_log_gaussian_prior))
        var sum_rho_div_rho_max = rho_div_rho_max_array.reduceLeft((x1, x2) => (x1 + x2))
        var log_gaussian_prior_sum = max_log_gaussian_prior + Math.log(sum_rho_div_rho_max)

        
        var log_rho_array = log_gaussian_prior_array.map(t => t - log_gaussian_prior_sum)
        var rho_array = log_rho_array.map(t => Math.exp(t))
    	
        var rho_x_array: Array[DenseVector[Double]] = new Array[DenseVector[Double]](mu.length)
        
        for(i <- 0 to (mu.length - 1)){
            rho_x_array(i) = x.map(t => t*rho_array(i))            
        }
        (x, rho_array, rho_x_array, log_gaussian_prior_sum)
    }
 
    def reduceForNewMeans(r1: (DenseVector[Double], Array[Double], Array[DenseVector[Double]], Double),
            			r2: (DenseVector[Double], Array[Double], Array[DenseVector[Double]], Double)):
            			(DenseVector[Double], Array[Double], Array[DenseVector[Double]], Double) = {
    	
        var sum_2 = new Array[Double](r1._2.length)
        
        var sum_3 = new Array[DenseVector[Double]](r1._2.length)
        for(i <- 0 to r2._3.length - 1){
            sum_2(i) = r1._2(i) + r2._2(i)
            sum_3(i) = r1._3(i) + r2._3(i)
        }
                
        (r1._1, sum_2, sum_3, r1._4 + r2._4)
    }
    
    def getNewCovar(x: (DenseVector[Double], Array[Double], Array[DenseVector[Double]], Double),
            			 means: Array[DenseVector[Double]]) : 
            			 Array[DenseMatrix[Double]] = {
        
        var covar_array = new Array[DenseMatrix[Double]](means.length)
        for(i <- 0 to covar_array.length - 1){
            var x_minus_mu = x._1 - means(i)
            covar_array(i) = (x_minus_mu*x_minus_mu.t)*x._2(i)
        }
        covar_array
    }
    
    def reduceForNewCovar(c1: Array[DenseMatrix[Double]],
            			  c2: Array[DenseMatrix[Double]]):
            			  Array[DenseMatrix[Double]] = {
        var covar_array = new Array[DenseMatrix[Double]](c1.length)
        
        for (i <- 0 to c1.length - 1){
            assert(c1(i).size == c2(i).size)
            covar_array(i) = c1(i) + c2(i)
            //println("unnormalized covar (" + i + ") = " + covar_array(i))
        }
        covar_array
    }
	
    def main(args: Array[String]){
        
        if(args.length < 4){
            System.err.println("Usage: SparkGMM <master> <input-dataset> <output-file> <init-means>")
            System.exit(1)
        }
        for(i <- 0 to (args.length - 1)){
        	println(args(i))
    	}
        
        
        var var_scale = 1
        var torlerance = Math.pow(10, -8)
        
        /* initialize parameters for mixture gaussians */
        var init_mean_file = io.Source.fromFile(args(3))
        var init_mean_lines = init_mean_file.getLines()
        var gaussian_means = init_mean_lines.map[DenseVector[Double]](string2Vector).toArray /* gaussian means, cluster centers */
        var num_gaussians = gaussian_means.length
        println(num_gaussians)
        init_mean_file.close()

        var gaussian_covars = Array.fill[DenseMatrix[Double]](num_gaussians){DenseMatrix.eye[Double](gaussian_means(0).size).map(_*var_scale)} /* gaussian variances */
        var gaussian_priors = Array.fill(num_gaussians){1/num_gaussians.toDouble} /* gaussian priors */

        for(i <- 0 to (num_gaussians - 1)){
            println(i)
            println(gaussian_means(i))
            println(gaussian_covars(i))
            println(gaussian_priors(i))
        }
        val sc = new SparkContext(args(0), "SparkGMM", "/home/user/jinlianw/ml-framework/spark", Array("/home/user/jinlianw/ml-framework/progs/biglearning_spark.jar"))
        
        //val sc = new SparkContext(args(0), "SparkGMM", "/h/jinlianw/ml-framework/spark/spark-0.6.0", Array("/h/jinlianw/ml-framework/spark/progs/biglearning_spark.jar"))
        
        //val sc = new SparkContext(args(0), "SparkGMM")
        
        var file = sc.textFile(args(1))
        
        var dataset = file.map[DenseVector[Double]](string2Vector).cache()
        
        var num_points = dataset.count()
        var old_ll = -1.0
        var ite_cnt = 0
        
        var threadMXBean : ThreadMXBean = ManagementFactory.getThreadMXBean()
        
        if(!threadMXBean.isCurrentThreadCpuTimeSupported()){
            
            System.err.println("ThreadCpuTime is not supported!\n")
        }else{
            if(!threadMXBean.isThreadCpuTimeEnabled()){
                threadMXBean.setThreadCpuTimeEnabled(true)
            }
        }
        val cpu_time_start = threadMXBean.getCurrentThreadCpuTime()
        val wall_time_start = System.currentTimeMillis()
        
        println("start cpu time = " + cpu_time_start)
        println("start wall time = " + wall_time_start)
        breakable{
        	while(true){
        	    
        	    if(ite_cnt >= 20){
        		    break
        		}
        	    
        	    var gaussian_covars_inverse = new Array[DenseMatrix[Double]](num_gaussians)
        	    
        	    var gaussian_covars_det = new Array[Double](num_gaussians)
        	    
        	    for(i <- 0 to (num_gaussians - 1)){
        	        gaussian_covars_inverse(i) = LinearAlgebra.inv(gaussian_covars(i))
        	        gaussian_covars_det(i) = LinearAlgebra.det(gaussian_covars(i))
        	    }
        	    
        		val rhos = dataset.map[(DenseVector[Double], Array[Double], Array[DenseVector[Double]], Double)](x => newResponsibilityEachX(x, gaussian_priors, gaussian_means, gaussian_covars_inverse, gaussian_covars_det))
        		
        		var new_rho_means : (DenseVector[Double], Array[Double], Array[DenseVector[Double]], Double) = rhos.reduce((x1, x2) => reduceForNewMeans(x1, x2))
        		
        		var new_ll = new_rho_means._4
        		
        		println("iteration = " + ite_cnt);
        		println("ll = " + new_ll);
        		
        		if(ite_cnt == 0){
        		    old_ll = new_ll
        		}else{
        		    println("new_ll - old_ll = " + (new_ll - old_ll));
        			if(new_ll - old_ll < torlerance){
        				break
        			}else{
        			    old_ll = new_ll
        			}
        			    
        		}
        		
        		
        		
        		ite_cnt += 1
        		
        		var new_rho_sum = new_rho_means._2
        		
        		var new_means = new Array[DenseVector[Double]](gaussian_priors.length)
        		
        	    var new_priors = new Array[Double](gaussian_priors.length)
        		
        		for (i <- 0 to new_means.length - 1){
        		    new_means(i) = new_rho_means._3(i)/new_rho_sum(i)
        		    
        		            		    
        		    new_priors(i) = new_rho_sum(i)/num_points
        		    
        		}
        		println("cluster centers: ")
        		
        		for(i <- 0 to new_means.length - 1){
        		    
        		    println(new_means(i))
        		} 
        		var covars = rhos.map[Array[DenseMatrix[Double]]]( x => getNewCovar(x, new_means))
        		
        		var new_covars = covars.reduce((x1, x2) => reduceForNewCovar(x1, x2))
        		
        		for(i <- 0 to new_covars.length - 1){
        			//println("unnormalized new_covars(" + i + ") = " + new_covars(i))
        		    new_covars(i) = new_covars(i)/new_rho_sum(i) + DenseMatrix.eye[Double](new_covars(i).rows)*(Math.pow(10, -6))
        			//println("normalized new_covars(" + i + ") = " + new_covars(i))
        		}
        		
        		gaussian_priors = new_priors
        	    
        		gaussian_means = new_means        		
        		
        		gaussian_covars = new_covars
        		
        	}
        }
        val cpu_time_end = threadMXBean.getCurrentThreadCpuTime()
        
        val cpu_time_total = (cpu_time_end - cpu_time_start)/Math.pow(10.0, 9)
        
        val wall_time_end = System.currentTimeMillis()
        
        val wall_time_total = (wall_time_end - wall_time_start)/Math.pow(10.0, 3)
        
        println("cpu_time_total = " + cpu_time_total + "(sec)")
        
        println("wall_time_total = " + wall_time_total + "(sec)")
        
        var fw : FileWriter = new FileWriter(args(2))
        
        println("count of iterations: " + ite_cnt)
        fw.write("**************** experiment stats *************\n")
        fw.write("dataset size: " + num_points + "\n")
        fw.write("number of clusters: " + gaussian_priors.length + "\n")
        
        fw.write("count of iterations: " + ite_cnt + "\n")
        
        fw.write("cpu_time_total = " + cpu_time_total + "(sec) \n")
        fw.write("wall_time_total = " + wall_time_total + "(sec) \n")
        fw.write("threads: " + args(0) + "\n")
        
        println("threads: " + args(0))
        fw.write("\n\n**************** experiment results *************\n")
        println("\ngaussian priors")
        fw.write("gaussian priors\n")
        for(i <- 0 to gaussian_priors.length - 1){
        		println(gaussian_priors(i))
        		fw.write("gaussian prior " + i + " : " + gaussian_priors(i) + "\n")
        }
        println("\ngaussian_means")
        for(i <- 0 to gaussian_means.length - 1){
        		println(gaussian_means(i))
        		fw.write("gaussian mean " + i + " : " + gaussian_means(i) + "\n")
        }
        println("\ngaussian_covars")
        for(i <- 0 to gaussian_covars.length - 1){
        		println(gaussian_covars(i))
        		fw.write("gaussian covar " + i + " : " + gaussian_covars(i) + "\n")
        }
        fw.close()
    }
}
